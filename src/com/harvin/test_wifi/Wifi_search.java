package com.harvin.test_wifi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Wifi_search extends Activity {
	WifiManager wifiManager;
	WifiReceiver wifiReceiver;
	
	//从intent获得的需要存储的mac地址
	List<String> macs_list;
	List<ScanResult> wifi_list;
	TextView mac_info;
	TextView x;
	TextView y;
    StringBuilder sb = new StringBuilder();
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wifi_search);
		
		//获得上一个activities的mac列表
//		Intent intent = getIntent();
//		macs_list	  = (List<String>)intent.getExtras().get("macs");
		
		//初始化控件
		mac_info	= (TextView)findViewById(R.id.mac_info);
		x			= (TextView)findViewById(R.id.x);
		y			= (TextView)findViewById(R.id.y);
		
		
		
		//为按钮添加点击事件。
		Button get_info = (Button)findViewById(R.id.get_info);
		Button write	= (Button)findViewById(R.id.write);
		get_info.setOnClickListener(new Get_info_listener());
		write.setOnClickListener(new write_listener());
		
	}
	
	
	
	//获取WiFi信息的事件
	private class Get_info_listener implements OnClickListener{

		@Override
		public void onClick(View arg0) {
			Context context = getApplicationContext();  
	        wifiManager = (WifiManager) context  
	                .getSystemService(Context.WIFI_SERVICE);  
	        //初始化WiFi service
			wifiReceiver= new WifiReceiver();
			registerReceiver(wifiReceiver,  new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

			
	        if(!wifiManager.isWifiEnabled()){
		    	Toast.makeText(Wifi_search.this, 
		    			"你特么是在逗我吗？\nWiFi没开，要测个毛线啊", 
		    			5000).show();
		    	System.exit(0);
		    }
			
			wifiManager.startScan();
		}
		
	}
	//写入文件的事件
	private class write_listener implements OnClickListener{
		
		@Override
		public void onClick(View v){
			if(!Environment.getExternalStorageState()
					.equals(Environment.MEDIA_MOUNTED)){
				Toast.makeText(Wifi_search.this, "没有发现内存卡",3000).show();
				System.exit(0);
			}
			
			
			File f = Environment.getExternalStorageDirectory();//获取SD卡目录
			
			File fileDir = new File(f,"wifi_info.txt");
			
			
			try {
				//FileOutputStream os = new FileOutputStream(fileDir);
				RandomAccessFile os = new RandomAccessFile(fileDir, "rw");
				long fileLength		= os.length();
				os.seek(fileLength);
			    os.write(sb.toString().getBytes());
			
			    os.close();
			
			    Toast.makeText(Wifi_search.this, "保存到SD卡中", 3000).show();
			
			} catch (IOException e) {
			
			    // TODO Auto-generated catch block
			
			    e.printStackTrace();
			
			}
			
			
		}
	}
	
	private class WifiReceiver extends BroadcastReceiver{
		public void onReceive(Context c, Intent intent) {
			
            wifi_list = wifiManager.getScanResults();
            sb.delete(0, sb.length());
            sb.append("t=0;pos="+x.getText()+","+
            		y.getText()+",0.0;");
            sb.append("id=0;");
            ScanResult scanResult;
            for (int i = 0; i < wifi_list.size(); i++){
            	scanResult = wifi_list.get(i);
                sb.append(scanResult.BSSID+"=");
                sb.append(scanResult.level+",0,2,-90;");
            }
            sb.append("\n");
            
            
            mac_info.setText("当前位置X:"+x.getText()
            		+"  Y:"+y.getText()+"\n"+sb);
                
        }
	}
}
