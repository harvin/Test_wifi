package com.harvin.test_wifi;

import java.util.ArrayList;

import android.os.Bundle;
import android.R.string;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class StartActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		
		Button start_button = (Button) findViewById(R.id.button1);
		start_button.setOnClickListener(new OnClickListener() {
								
			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent(StartActivity.this, Wifi_search.class);
				intent.putExtra("macs", getMacInfos());
				startActivity(intent);
			}
			
		});
		
	}
	
	public ArrayList<String> getMacInfos() {
		
		ArrayList<String> macsArrayList = new ArrayList<String>();
		
		EditText editText;
		
		editText = (EditText)findViewById(R.id.mac1);
		macsArrayList.add(editText.getText().toString());
		
		editText = (EditText)findViewById(R.id.mac2);
		macsArrayList.add(editText.getText().toString());

		editText = (EditText)findViewById(R.id.mac3);
		macsArrayList.add(editText.getText().toString());
		
		editText = (EditText)findViewById(R.id.mac4);
		macsArrayList.add(editText.getText().toString());
		
		editText = (EditText)findViewById(R.id.mac5);
		macsArrayList.add(editText.getText().toString());
		
		return macsArrayList;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.start, menu);
		return true;
	}

}
